package com.example.demo.service;

import com.example.demo.entity.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;


public interface UserService{
    User addUser(User user);

    User updateUser(User user);

    User findByUsername(String username);
    List<User> allUsers();
    boolean deleteUser(Long userId);

    List<User> usergtList(Long idMin);
}
