package com.example.demo.controller;

import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class AdminController {
    Logger log = LoggerFactory.getLogger(AdminController.class);
    @Autowired
    private UserService userService;

    @GetMapping("/hello")
    public ModelAndView hello(){
        return new ModelAndView("hello");
    }

    @GetMapping("/admin")
    public ModelAndView userList(Model model){
        model.addAttribute("allUsers", userService.allUsers());
        log.info("In admin Page - visited Landing Page");
        return new ModelAndView("admin");
    }

    @GetMapping("/403")
    public String accessDenied() {
        log.info("In 403 Page - visited Landing Page");
        return "403";
    }

    @GetMapping("/update")
    public String updateUser(String username,Model model) {
        model.addAttribute("updateUser", userService.findByUsername(username));
        log.info("In update Page - visited Landing Page");
        return "update";
    }

    @PostMapping("/adminUpdate")
    public String updateUser(@ModelAttribute("updateUser") User user, Model model) {
        userService.updateUser(user);
        return "redirect:/admin";
    }

    @PostMapping("/adminDelete")
    public String  deleteUser(@ModelAttribute("allUsers") User user, Model model, RedirectAttributes redirAttrs) {

        //userService.deleteUser(user.getId());
        if (!userService.deleteUser(user.getId())) {
            redirAttrs.addFlashAttribute("error", "You can't delete yourself");
            log.info("Deleted User - visited Landing Page");
            return "redirect:/admin";
        }
        return "redirect:/admin";
    }

    @GetMapping("/admin/gt/{userId}")
    public String  gtUser(@PathVariable("userId") Long userId, Model model) {
        model.addAttribute("allUsers", userService.usergtList(userId));
        return "admin";
    }
}
