package com.example.demo.controller;

import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.BeanDefinitionDsl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
//import org.apache.log4j.Logger;


import java.util.*;

@Controller
public class RegistrationController {
    Logger log = LoggerFactory.getLogger(RegistrationController.class);

    private UserService userService;
    private RoleRepository roleRepository;
    private UserRepository userRepository;

    @Autowired
    public RegistrationController(UserService userService, RoleRepository roleRepository, UserRepository userRepository) {
        this.userService = userService;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/")
    public String showPage(Model model){
        log.info("In Main Page - visited Landing Page");
        return "main";
    }
    @GetMapping("/registration")
    public String registration(Model model){
        log.info("In Registration Page - visited Landing Page");
        model.addAttribute("user", new User());
        List<Role> roles = (List<Role>) roleRepository.findAll();
        model.addAttribute("allRoles", roles);
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@ModelAttribute("user") User user, Model model) {
        User userFromDB = userRepository.findByUsername(user.getUsername());
        userService.addUser(user);
        log.info("In Registration Page - Created new User");
        log.debug(String.format("In Registration Page - Created new User = ", user.getUsername()));
        return "main";
    }
}
